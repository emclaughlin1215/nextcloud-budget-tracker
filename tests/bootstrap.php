<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Erin McLaughlin <emclaughlin@fastmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

require_once __DIR__ . '/../../../tests/bootstrap.php';
