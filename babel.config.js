// SPDX-FileCopyrightText: Erin McLaughlin <emclaughlin@fastmail.com>
// SPDX-License-Identifier: AGPL-3.0-or-later
const babelConfig = require('@nextcloud/babel-config')

module.exports = babelConfig
